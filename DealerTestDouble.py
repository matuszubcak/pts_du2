from typing import List

from DealerInterface import DealerInterface
from GameInterface import GameInterface
from Game import Game
from Hand import Hand
from HandInterface import HandInterface
from Trick import Trick
from Card import PokerCard
from GenTestDouble import GenDouble

class DealerTestDouble(DealerInterface):

    def newDeck(self, deck: List[int]):
        self._deck = deck
        self._generator = GenDouble()
        for x in deck:
            self._generator.push((x,"♥"))

    def create_game(self, number_of_players: int) -> GameInterface:
        trick: Trick = Trick(number_of_players)
        hands: List[HandInterface] = []

        for i in range(number_of_players):
            hand: Hand = Hand(i,[PokerCard(self._generator) for x in range(6)] ,trick)
            hands.append(hand)

        return Game(number_of_players, hands, trick)

    def game_type(self) -> str:
        return "Poker game tester"