import queue
from typing import Tuple

from GeneratorInterface import CardGenInterface

class GenDouble(CardGenInterface):
    def __init__(self):
        self._buffer = queue.SimpleQueue()


    def push(self, item: Tuple[int, str]):
        self._buffer.put(item)

    def next(self) -> Tuple[int, str]:
        return self._buffer.get()

