import queue
import random
from typing import Set

from GeneratorInterface import CardGenInterface


class SetCardGeneratorDouble(CardGenInterface):
    def __init__(self):
        self._buffer = queue.SimpleQueue()

    def push(self, set: Set):
        self._buffer.put(set)

    def next(self) -> Set[int]:
        return self._buffer.get()