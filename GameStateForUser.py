from typing import List

from GameStateInterface import GameStateInterface


class GameStateForUser(GameStateInterface):
    # Game has GameState not GameState interface, because it always in every version works with GameState
    # GameStateInterface unifies GameState and GameStateForUser, which does not store classes, only strings
    # Player does not need to have stored date in GameState, he just needs to see the game
    # Also it would be unwise to allow player to amend GameState via GameState setters and getters

    def __init__(self, cards: List[List[str]], trick: List[str]):
        self._cards: List[List[str]] = cards
        self._trick: List[str] = trick

    def cards(self) -> List[List[str]]:
        return self._cards

    def trick(self) -> List[str]:
        return self._trick