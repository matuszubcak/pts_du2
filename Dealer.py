from typing import List

from Card import PokerCard
from CardGen import CardGenerator
from DealerInterface import DealerInterface
from Game import Game
from GameInterface import GameInterface
from Hand import Hand
from HandInterface import HandInterface
from Trick import Trick


class Dealer(DealerInterface):

    def __init__(self):
        self._generator: CardGenerator = CardGenerator()

    def create_game(self, number_of_players: int) -> GameInterface:
        trick: Trick = Trick(number_of_players)
        hands: List[HandInterface] = []

        for i in range(number_of_players):
            hand: HandInterface = Hand(i,[PokerCard(self._generator) for x in range(6)] ,trick)
            hands.append(hand)

        return Game(number_of_players, hands, trick)

    def game_type(self) -> str:
        return "Poker game"