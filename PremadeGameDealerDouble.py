from typing import List

from Card import PokerCard
from DealerInterface import DealerInterface
from DealerTestDouble import DealerTestDouble
from Game import Game
from GameInterface import GameInterface
from Hand import Hand
from Trick import Trick
from GenTestDouble import GenDouble


class PremadeGameDealerDouble(DealerInterface):

    def create_game(self, number_of_players: int) -> GameInterface:
        trick: Trick = Trick(number_of_players)
        hands: List[Hand] = []

        deck2: List[int] = [2, 2, 5, 6, 14, 13, 12, 12, 10, 9, 7, 3, 6, 6, 6, 2, 4, 13, 12, 13, 7, 8, 14, 14]
        dealerDouble = DealerTestDouble()
        dealerDouble.newDeck(deck2)
        game = dealerDouble.create_game(number_of_players)
        return game


    def game_type(self) -> str:
        return "TEST"
