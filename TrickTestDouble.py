import queue

from CardInterface import CardInterface
from TrickInterface import TrickInterface

#Trick Testing Double
class TrickTestDouble(TrickInterface):
    def __init__(self):
        self._buffer = queue.SimpleQueue()

    def push(self, retval: bool):
        self._buffer.put(retval)

    #returns always true because lowest card should (almost) always be added to the trick
    #this can be used for our advantage to test, whether Hand is passing lowest card properly
    def play_as_lowest(self, c: CardInterface, p: int) -> bool:
            self._buffer.get()
            return True

    #Returns next bool value from buffer
    def play_normal(self, c: CardInterface, p: int) -> bool:
            return self._buffer.get()