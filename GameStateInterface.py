from typing import List


class GameStateInterface:

    def cards(self) -> List[List[str]]:
        pass

    def trick(self) -> List[str]:
        pass