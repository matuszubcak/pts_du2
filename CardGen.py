import random
from typing import Tuple, List
from GeneratorInterface import CardGenInterface


class CardGenerator(CardGenInterface):
    def __init__(self):
        symbols: str = "♥♦♣♠"
        self._remainingCards: List[Tuple[int,str]] = [ (x+2, y) for x in range(13) for y in symbols ]
        random.shuffle(self._remainingCards)


    def next(self) -> Tuple[int,str]:
        if self._remainingCards:
            return self._remainingCards.pop()
        else:
            raise Exception("Empty card deck exception")

    def hasNext(self) -> bool:
        return bool(self._remainingCards)