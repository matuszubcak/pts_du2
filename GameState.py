from typing import List

from GameStateInterface import GameStateInterface
from HandInterface import HandInterface
from TrickInterface import TrickInterface


class GameState(GameStateInterface):
    # Game has GameState not GameState interface, because it always in every version works with GameState
    # GameStateInterface unifies GameState and GameStateForUser, which does not store classes, only strings
    # Player does not need to have stored date in GameState, he just needs to see the game
    # Also it would be unwise to allow player to amend GameState via GameState setters and getters

    def __init__(self, hands: List[HandInterface], trick: TrickInterface):
        self._hands: List[HandInterface] = hands
        self._trick: TrickInterface = trick

    def setTrick(self, trick: TrickInterface):
        self._trick = trick

    def setHand(self, playerno: int, hand: HandInterface):
        self._hands[playerno] = hand

    def getHand(self, playerno: int) -> HandInterface:
        return self._hands[playerno]

    def getTrick(self) -> TrickInterface:
        return self._trick

    def cards(self) -> List[List[str]]:
        retMatrix: List[List[str]] = []
        for x in self._hands:
            retMatrix.append(x.cards())
        return retMatrix

    def trick(self) -> List[str]:
        return self._trick.cards()