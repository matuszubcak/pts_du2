from GameState import GameState


class GameInterface:

    def play(self, player: int, cardno: int) -> bool:
        pass

    def state(self) -> GameState:
        pass