import random
from typing import Set, List

from GeneratorInterface import CardGenInterface


class SetCardGenerator(CardGenInterface):
    def __init__(self):
        self._vals: List[int] = [1, 2, 3, 4, 5, 6, 7]


    def next(self) -> Set[int]:
        cardSet: Set = set()
        for x in self._vals:
            if random.choice([True, False]):
                cardSet.add(x)
        return cardSet