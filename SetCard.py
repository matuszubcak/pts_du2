from __future__ import annotations

from CardInterface import CardInterface
from GeneratorInterface import CardGenInterface


class SetCard(CardInterface):
    def __init__(self, generator: CardGenInterface):
        self._value = generator.next()


    def string(self) -> str:
        #for some reason we have to do it this way.
        #previously when set was empty it returned
        # set()
        # as empty set representation, which looks dumb for the user
        if self._value != set():
            return str(self._value)
        else:
            return "{}"

    def is_ge(self, other: CardInterface) -> bool:
        for x in other._value:
            if not x in self._value:
                return False
        return True




