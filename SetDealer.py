from typing import List

from Card import PokerCard
from DealerInterface import DealerInterface
from Game import Game
from GameInterface import GameInterface
from Hand import Hand
from HandInterface import HandInterface
from HighestFirstWinnerTrick import HighestFirstWinnerTrick
from SetCard import SetCard
from SetCardGenerator import SetCardGenerator



class SetDealer(DealerInterface):

    def __init__(self):
        self._generator: SetCardGenerator = SetCardGenerator()

    def create_game(self, number_of_players: int) -> GameInterface:
        trick: HighestFirstWinnerTrick = HighestFirstWinnerTrick(number_of_players)
        hands: List[HandInterface] = []

        for i in range(number_of_players):
            hand: HandInterface = Hand(i,[SetCard(self._generator) for x in range(6)] ,trick)
            hands.append(hand)

        return Game(number_of_players, hands, trick)

    def game_type(self) -> str:
        return "Set game"