from __future__ import annotations

from CardInterface import CardInterface
from GeneratorInterface import CardGenInterface


class PokerCard(CardInterface):
    def __init__(self, generator: CardGenInterface):
        card = generator.next()
        self._value: int = card[0]
        self._type: str = card[1]

    def string(self) -> str:
        cardString = str(self._value)
        if self._value == 11:
            cardString = "J"
        if self._value == 12:
            cardString = "Q"
        if self._value == 13:
            cardString = "K"
        if self._value == 14:
            cardString = "A"
        cardString += " " + self._type
        return cardString

    def is_ge(self, other: CardInterface) -> bool:
        return self._value >= other._value



