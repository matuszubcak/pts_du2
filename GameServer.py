from typing import List

from Dealer import Dealer
from DealerInterface import DealerInterface
from GameInterface import GameInterface
from GameServerInterface import GameServerInterface
from GameState import GameState
from GameStateForUser import GameStateForUser
from GameStateInterface import GameStateInterface
from PremadeGameDealerDouble import PremadeGameDealerDouble
from SetDealer import SetDealer


class GameServer(GameServerInterface):

    def __init__(self, number_of_players):
        self._number_of_players: int = number_of_players
        self._game: GameInterface = None

        #UPDATE THIS WHEN NEW DEALER = NEW GAME TYPE IS CREATED !!!
        self._dealerPool: List[DealerInterface] = [Dealer(), PremadeGameDealerDouble(), SetDealer()]


    def play(self, player: int, cardno: int) -> bool:
        if self._game is None:
            return False

        return self._game.play(player, cardno)

    def state(self, player: int) -> GameStateInterface:
        if self._game is None or player >= self._number_of_players:
            raise Exception("Wrong state request exception")

        state: GameState = self._game.state()
        hand_strings: List[List[str]] = []
        hand_strings.append(state.getHand(player).cards())
        for i in range( self._number_of_players ):
            if i != player:
                hand_str = state.getHand(i).cards()
                for j in range (6):
                    if hand_str[j] != "":
                        hand_str[j] = "?"
                hand_strings.append(hand_str)
        return GameStateForUser(hand_strings, state.trick())



    def new_game(self, type: str) -> bool:
        for x in self._dealerPool:
            if x.game_type() == type:
                self._game = x.create_game(self._number_of_players)
                return True
        return False
