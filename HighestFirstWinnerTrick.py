from typing import List
from CardInterface import CardInterface
from TrickInterface import TrickInterface

class HighestFirstWinnerTrick(TrickInterface):

    def __init__(self, number_of_players: int):
        self._cards: List[CardInterface] = []
        self._winner: int = -1
        self._number_of_players = number_of_players

    def _highest(self) -> CardInterface:
        highest: CardInterface = self._cards[0]
        for x in self._cards:
            if x.is_ge(highest):
                highest = x
        return highest

    def play_as_lowest(self, c: CardInterface, p: int) -> bool:
        if(self.size() < self._number_of_players):
            highest = self._highest()
            self._cards.append(c)
            if self.size() == 0:
                self._winner = p
            if c.is_ge(highest) and not highest.is_ge(c):
                self._winner = p
            return True
        return False

    def play_normal(self, c: CardInterface, p: int) -> bool:
        if self.size() == 0:
            self._cards.append(c)
            self._winner = p
            return True

        if(self.size() < self._number_of_players):
            highest: CardInterface = self._highest()
            if c.is_ge(highest):
                self._cards.append(c)
                if not highest.is_ge(c):
                    self._winner = p
                return True
            return False

        return False

    def winner(self) -> int:
        return self._winner

    def size(self) -> int:
        return len(self._cards)

    def cards(self) -> List[str]:
        retList: List[str] = []
        for x in self._cards:
            retList.append(x.string())
        while len(retList) != self._number_of_players:
            retList.append("")
        return retList

    def reset(self):
        self._cards = []
        self._winner = -1