import random
from typing import List

from GameInterface import GameInterface
from GameState import GameState
from HandInterface import HandInterface
from TrickInterface import TrickInterface

class Game(GameInterface):

    def __init__(self, number_of_players: int, hands: List[HandInterface], trick: TrickInterface):
        #Game has GameState not GameState interface, because it always in every version works with GameState
        #GameStateInterface unifies GameState and GameStateForUser, which does not store classes, only strings
        #Player does not need to have stored date in GameState, he just needs to see the game
        #Also it would be unwise to allow player to amend GameState via GameState setters and getters
        self._gameState: GameState = GameState(hands, trick)
        self._number_of_players: int = number_of_players
        self._trick_started_by: int = 0
        self._turn: int = 0
        self._actingPlayer: int = self._trick_started_by

    def play(self, player: int, cardno: int) -> bool:
        if player >= self._number_of_players or player != self._actingPlayer:
            return False

        if self._turn == 5:
            return False

        # we do this here not after the turn, because we want to let the next (winner) player know
        # what was played last turn and how the final trick looks like
        trick: TrickInterface = self._gameState.getTrick()
        if trick.size() == self._number_of_players:
            trick.reset()
            self._gameState.setTrick(trick)

        #if play is successful, card is discarded from hand and added to trick automatically
        #we must pass control of the game to next player though
        retval:bool = self._gameState.getHand(player).play(cardno)
        if retval:
            self._actingPlayer = (self._actingPlayer + 1) % self._number_of_players

        #if trick is already full, we must pass control of the game to the trick winner
        trick = self._gameState.getTrick()
        if trick.size() == self._number_of_players:
            self._trick_started_by = trick.winner()
            self._actingPlayer = trick.winner()
            self._turn += 1

        return retval


    def state(self) -> GameState:
        return self._gameState