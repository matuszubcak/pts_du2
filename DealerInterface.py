from GameInterface import GameInterface


class DealerInterface:

    def create_game(self, number_of_players: int) -> GameInterface:
        pass

    def game_type(self) -> str:
        pass