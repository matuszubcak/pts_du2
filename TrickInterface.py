from typing import List

from CardInterface import CardInterface


class TrickInterface():

    def play_as_lowest(self, c: CardInterface, p: int) -> bool:
        pass

    def play_normal(self, c: CardInterface, p: int) -> bool:
        pass

    def winner(self) -> int:
        pass

    def size(self) -> int:
        pass

    def cards(self) -> List[str]:
        pass

    def reset(self):
        pass