from typing import List

from HandInterface import HandInterface
from TrickInterface import TrickInterface
from CardInterface import CardInterface

class Hand(HandInterface):

    def __init__(self, player: int, cards: List[CardInterface], trick: TrickInterface):
        if len(cards) != 6:
            raise Exception("Improper starting amound of cards for player " + str(player) + " exception")
        self._player: int = player
        self._cards: List[CardInterface] = cards
        self._trick: TrickInterface = trick

    def _empty(self) -> bool:
        return not self._cards

    def _lowest(self) -> List[CardInterface]:
        if not self._empty():
            lowest: List[CardInterface] = []
            lowest.append(self._cards[0])
            for x in self._cards:
                isLowest = False
                isLowEnoughToBeAppended = False

                for y in lowest:
                    if y.is_ge(x):
                        if x.is_ge(y):
                            isLowEnoughToBeAppended = True
                        else:
                            isLowest = True

                if isLowest:
                    lowest.clear()
                    lowest.append(x)
                elif isLowEnoughToBeAppended:
                    lowest.append(x)

            return lowest
        else:
            raise Exception("Empty hand exception")

    def _remove(self, cardno: int):
        while(cardno < len(self._cards) - 1):
            self._cards[cardno], self._cards[cardno + 1] = self._cards[cardno + 1], self._cards[cardno]
            cardno += 1
        self._cards.pop()

    def play(self, cardno: int) -> bool:
        if cardno >= len(self._cards):
            return False

        card: CardInterface = self._cards[cardno]
        retval: bool
        if card in self._lowest():
            retval = self._trick.play_as_lowest(card, self._player)
            if retval:
                self._remove(cardno)
            return retval
        else:
            retval = self._trick.play_normal(card, self._player)
            if retval:
                self._remove(cardno)
            return retval

    def cards(self) -> List[str]:
        retList: List[str] = []
        for x in self._cards:
            retList.append(x.string())
        while  len(retList) != 6:
            retList.append("")
        return retList
