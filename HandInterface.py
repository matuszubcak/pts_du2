from typing import List


class HandInterface:

    def play(self, cardno: int) -> bool:
        pass

    def cards(self) -> List[str]:
        pass